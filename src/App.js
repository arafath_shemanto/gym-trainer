import { Route, Routes } from "react-router-dom";
import About from "./components/About/About";
import Blog from "./components/Blog/Blog";
import Checkout from "./components/Checkout/Checkout";
import Error_404 from "./components/Error/Error_404";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import Home from "./components/Home/Home";
import Login from "./components/Login/Login";
import Register from "./components/Register/Register";
import Requireauth from "./components/Reruireauth/Requireauth";
import Services from "./components/Services/Services";

function App() {
  return (
    <div className="App">
      <Header></Header>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/home" element={<Home />} />
        <Route path="/services" element={<Services />} />
        <Route path="/blog" element={<Blog />} />
        <Route path="/about" element={<About />} />
        <Route path="/register" element={<Register />} />
        <Route
          path="/checkout"
          element={
            <Requireauth>
              <Checkout />
            </Requireauth>
          }
        />
        <Route path="/login" element={<Login />} />
        <Route path="*" element={<Error_404 />} />
      </Routes>
      <Footer></Footer>
    </div>
  );
}

export default App;
