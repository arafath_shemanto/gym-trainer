import React from "react";
import { useNavigate } from "react-router-dom";

const Service = ({ service }) => {
  const navigate = useNavigate();
  const navigateHandler = () => {
    navigate("/checkout");
  };
  const { img, description, prise } = service;
  return (
    <div className="col-xl-4 col-lg-4 col-md-6">
      <div className="service_box mb_30">
        <div className="thumb">
          <img className="img-fluid" src={img} alt="" />
        </div>
        <div className="service_desc">
          <h4>{description}</h4>
          <p>prise start from : ${prise}</p>
          <button onClick={navigateHandler} className="primary_btn small_btn">
            get it now
          </button>
        </div>
      </div>
    </div>
  );
};

export default Service;
