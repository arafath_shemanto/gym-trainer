import React from "react";

const Checkout = () => {
  return (
    <div className="section_spacing">
      <div className="container">
        <div className="row">
          <div className="col-12 text-center">
            <h3 className="fs-4 fw-bold text-uppercase">checkout</h3>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Checkout;
