import React from "react";

const Blog = () => {
  return (
    <div className="section_spacing">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="theme_title mb_40">
              <h4 className="fs-4">
                Difference between authorization and authentication
              </h4>
              <p>
                <strong>authorization : </strong>
                it determines whether you are authorized to access the
                resources.Process of verifying whether access is allowed or
                not.Determines what user can and cannot access.
              </p>
              <p>
                <strong>authentication : </strong>
                authentication is all about validating . its confirms user
                identity to grant access to the system.authentication is the
                process of validating user credentials to gain user access.
                Authentication usually requires a username and a password.
              </p>
            </div>
          </div>
          <div className="col-12">
            <div className="theme_title mb_40">
              <h4 className="fs-4">
                Why are you using firebase? What other options do you have to
                implement authentication?
              </h4>
              <p>
                <strong>Why are you using firebase? : </strong> I think it is
                easy to use for me, and it is from a reputed company so for
                security, i love to use that
              </p>
              <p>
                i Think there is a lot of option of authentication , exaple like
                cards, retina scans, voice recognition, and fingerprints.etc
              </p>
            </div>
          </div>
          <div className="col-12">
            <div className="theme_title">
              <h4 className="fs-4">
                What other services does firebase provide other than
                authentication
              </h4>
              <ul>
                <li>1. Cloud Firestore</li>
                <li>2. Cloud Functions</li>
                <li>3. Hosting</li>
                <li>4. Cloud Messaging</li>
                <li>5.Remote Config</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Blog;
