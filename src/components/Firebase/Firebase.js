// Import the functions you need from the SDKs you need

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAXvovB45WUDNfebTsn1ISAtcYcdyQSUJQ",
  authDomain: "gym-trainer-c40fa.firebaseapp.com",
  projectId: "gym-trainer-c40fa",
  storageBucket: "gym-trainer-c40fa.appspot.com",
  messagingSenderId: "386573748955",
  appId: "1:386573748955:web:d0a4d15c323433e7a11afe",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
export default auth;
