import React from "react";
import myimg from "../../assets/img/arafath_shemanto.jpg";
const About = () => {
  return (
    <div className="section_spacing">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="theme_title mb_30">
              <h3>about me</h3>
            </div>
          </div>
          <div className="col-md-4">
            <div className="thumb">
              <img src={myimg} alt="" className="img-fluid" />
            </div>
          </div>
          <div className="col-md-8">
            <div className="content">
              <h3 className="text-capitalize fs-4 fw-bold">my goals</h3>
              <p>
                I have already learned HTML, CSS, SASS, JS, DOM, Jquery,
                BooTstrap, and Tailwind. Now i am learning React js. Now my full
                focus is on React js. After Leaning React Perfectly i want to
                make web applications using reactJS.I want to be a full-stack
                software developer.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
