import React from "react";
import img1 from "../../assets/img/personal/blog_1.jpg";
import img2 from "../../assets/img/personal/blog_2.jpg";
import img3 from "../../assets/img/personal/blog_3.jpg";
import img4 from "../../assets/img/personal/blog_4.jpg";
const Personal = () => {
  return (
    <div className="section_spacing pt-0">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="theme_title text-center mb_40">
              <h3>My Resent blogs</h3>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-6">
            <div class="home9_blog_Widget mb_30">
              <a href="#" class="thumb">
                <img src={img1} alt="" />
              </a>
              <div class="blog_content">
                <div class="date_text">January 14, 2021</div>
                <a href="#">
                  <h4>Thinking beyond influent social media offer</h4>
                </a>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="home9_blog_Widget mb_30">
              <a href="#" class="thumb">
                <img src={img2} alt="" />
              </a>
              <div class="blog_content">
                <div class="date_text">January 14, 2021</div>
                <a href="#">
                  <h4>Thinking beyond influent social media offer</h4>
                </a>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="home9_blog_Widget mb_30">
              <a href="#" class="thumb">
                <img src={img3} alt="" />
              </a>
              <div class="blog_content">
                <div class="date_text">January 14, 2021</div>
                <a href="blog_details.php">
                  <h4>Thinking beyond influent social media offer</h4>
                </a>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="home9_blog_Widget mb_30">
              <a href="#" class="thumb">
                <img src={img4} alt="" />
              </a>
              <div class="blog_content">
                <div class="date_text">January 14, 2021</div>
                <a href="#">
                  <h4>Thinking beyond influent social media offer</h4>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Personal;
