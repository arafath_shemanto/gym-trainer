import { signOut } from "firebase/auth";
import React from "react";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import { useAuthState } from "react-firebase-hooks/auth";
import { Link } from "react-router-dom";
import logo from "../../assets/img/logo.png";
import auth from "../Firebase/Firebase";

const Header = () => {
  const [user, loading, error] = useAuthState(auth);
  const logout = () => {
    signOut(auth);
  };
  return (
    <Navbar
      className="header_area"
      collapseOnSelect
      expand="lg"
      bg="light"
      variant="light"
    >
      <Container>
        <Navbar.Brand href="#home">
          <img src={logo} alt="" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className=" main_menu navbar-nav flex-fill justify-content-center">
            <Link to="/">Home</Link>
            <Link to="/services">services</Link>
            <Link to="/blog">blog</Link>
            <Link to="/about">about</Link>
          </Nav>
          <Nav className="g-2">
            {user ? (
              <button
                onClick={logout}
                className="primary_btn small_btn text-center"
              >
                log out
              </button>
            ) : (
              <div>
                <Link className="primary_btn small_btn text-center" to="/login">
                  Login
                </Link>
              </div>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Header;
