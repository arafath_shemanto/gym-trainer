import React from "react";

const Footer = () => {
  return (
    <footer className="home_one_footer">
      <div className="main_footer_wrap">
        <div className="container">
          <div className="row">
            <div className="col-xl-2 col-lg-3 col-md-6">
              <div className="footer_widget">
                <div className="footer_title">
                  <h3>Information</h3>
                </div>
                <ul className="footer_links">
                  <li>
                    <a href="my_order.php">Delivery information</a>
                  </li>
                  <li>
                    <a href="faq.php">Privacy Policy</a>
                  </li>
                  <li>
                    <a href="product.php">Sales</a>
                  </li>
                  <li>
                    <a href="faq.php">Terms & Conditions</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-xl-2 col-lg-3 col-md-6">
              <div className="footer_widget">
                <div className="footer_title">
                  <h3>Account</h3>
                </div>
                <ul className="footer_links">
                  <li>
                    <a href="about.php">About Us</a>
                  </li>
                  <li>
                    <a href="product.php">Brands</a>
                  </li>
                  <li>
                    <a href="product.php">Gift Vouchers</a>
                  </li>
                  <li>
                    <a href="contact.php">Site Map</a>
                  </li>
                  <li>
                    <a href="product.php">Accessories</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-3 col-xl-2  col-md-6">
              <div className="footer_widget">
                <div className="footer_title">
                  <h3>STORE</h3>
                </div>
                <ul className="footer_links">
                  <li>
                    <a href="product.php">Affiliate</a>
                  </li>
                  <li>
                    <a href="product.php">Bestsellers</a>
                  </li>
                  <li>
                    <a href="product.php">Discount</a>
                  </li>
                  <li>
                    <a href="product.php">Latest products</a>
                  </li>
                  <li>
                    <a href="product.php">Sale</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-2 col-xl-3 col-md-6">
              <div className="footer_widget">
                <div className="footer_title">
                  <h3>Need help</h3>
                </div>
                <h4 className="large_address">0020 500 - Infix - 000</h4>
                <ul className="footer_links mb_20">
                  <li>
                    <a href="#">Monday - Friday: 9:00 - 20:00</a>
                  </li>
                  <li>
                    <a href="#">Saturday: 11:00 - 15:00</a>
                  </li>
                </ul>
                <a href="#" className="green_round_btn">
                  info@codethemes.com
                </a>
              </div>
            </div>
            <div className="col-lg-2  col-md-6">
              <div className="footer_widget">
                <div className="footer_title">
                  <h3>Our Store</h3>
                </div>
                <p className="address_text">
                  1487 Rocky Horse Carrefour Arlington, TX 16819
                </p>
                <div className="social__Links">
                  <a href="#">
                    <i className="fab fa-twitter"></i>
                  </a>
                  <a href="#">
                    <i className="fab fa-linkedin-in"></i>
                  </a>
                  <a href="#">
                    <i className="fab fa-instagram"></i>
                  </a>
                  <a href="#">
                    <i className="fab fa-facebook"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="copyright_area">
        <div className="container">
          <div className="footer_border"></div>
          <div className="row">
            <div className="col-md-6">
              <div className="copy_right_text">
                <p>
                  © 2021 <a href="#"> gym Trainer</a>. All rights reserved. Made
                  By <a href="#">Arafath</a>.
                </p>
              </div>
            </div>
            <div className="col-md-6">
              <div className="payment_imgs ">
                <img src="img/payment_img.png" alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
