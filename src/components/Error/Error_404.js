import React from "react";
import error_img from "../../assets/img/error_img.png";
const Error_404 = () => {
  return (
    <div className="error_area">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="error_inner">
              <div className="thumb">
                <img className="img-fluid" src={error_img} alt="" />
              </div>
              <h3>Opps! Page Not Found</h3>
              <p>
                Perhaps you can try to refresh the page, sometimes it works.
              </p>
              <a
                href="index.php"
                className="primary_btn large_btn f_w_700 rounded-0"
              >
                + back to homepage
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Error_404;
