import React from "react";
import Banner from "../Banner/Banner";
import Personal from "../Personal/Personal";
import Services from "../Services/Services";

const Home = () => {
  return (
    <div>
      <Banner></Banner>
      <Services></Services>
      <Personal></Personal>
    </div>
  );
};

export default Home;
