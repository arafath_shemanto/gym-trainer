import React from "react";
import banner1 from "../../assets/img/banner.png";
const Banner = () => {
  return (
    <div className="slider_area">
      <div
        className="single_slider  d-flex align-items-center  overlay"
        style={{ backgroundImage: `url(${banner1})` }}
      >
        <div className="container">
          <div className="row align-items-center justify-content-center">
            <div className="col-xl-12">
              <div className="slider_text text-center">
                <span>Build Up Your</span>
                <h3>Body Shape</h3>
                <p>Build Your Body and Fitness with Professional Touch</p>
                <a href="#" className="primary_btn2">
                  Join me
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Banner;
