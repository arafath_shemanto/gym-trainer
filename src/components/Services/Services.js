import React, { useEffect, useState } from "react";
import Service from "../Service/Service";

const Services = () => {
  const [services, setService] = useState([]);
  useEffect(() => {
    fetch(`services.json`)
      .then((res) => res.json())
      .then((data) => setService(data));
  });
  return (
    <div className="section section_spacing">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="theme_title text-center mb_40">
              <h3>my services</h3>
            </div>
          </div>
        </div>
        <div className="row">
          {services.map((service) => (
            <Service key={service.id} service={service}></Service>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Services;
