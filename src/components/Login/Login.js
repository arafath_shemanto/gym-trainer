import React, { useState, useEffect } from "react";
import {
  useAuthState,
  useSendPasswordResetEmail,
  useSignInWithEmailAndPassword,
} from "react-firebase-hooks/auth";
import { Link, useLocation, useNavigate } from "react-router-dom";
import logo from "../../assets/img/logo.png";
import auth from "../Firebase/Firebase";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useSignInWithGoogle } from "react-firebase-hooks/auth";
const Login = () => {
  const [userState] = useAuthState(auth);

  const [signInWithEmailAndPassword, user, loading, error] =
    useSignInWithEmailAndPassword(auth);

  // gogole user create
  const [signInWithGoogle, Googleuser, Googleloading, GoogleError] =
    useSignInWithGoogle(auth);

  // pasward reset
  const [sendPasswordResetEmail, sending, PasswordReseterror] =
    useSendPasswordResetEmail(auth);

  const navigate = useNavigate();
  // location
  let location = useLocation();
  let from = location.state?.from?.pathname || "/";

  // set user information
  const [userInfo, setUserInfo] = useState({
    email: "",
    password: "",
  });

  const [userError, setUserError] = useState({
    emailError: "",
    passwordError: "",
    otherError: "",
  });

  // Handlers
  const emailHandler = (e) => {
    let emailRx = /\S+@\S+\.\S+/;
    let validEmail = emailRx.test(e.target.value);
    if (validEmail) {
      setUserInfo({ ...userInfo, email: e.target.value });
      setUserError({ ...userError, emailError: "" });
    } else {
      setUserInfo({ ...userInfo, email: "" });
      setUserError({ ...userError, emailError: "type valid email" });
    }
  };
  const passwordHandler = (e) => {
    let passwordRegex = /.{8,}/;
    let validPass = passwordRegex.test(e.target.value);
    if (validPass) {
      setUserInfo({ ...userInfo, password: e.target.value });
      setUserError({ ...userError, passwordError: "" });
    } else {
      setUserInfo({ ...userInfo, password: "" });
      setUserError({ ...userError, passwordError: "Must be 8 Charectar " });
    }
  };
  const loginHandler = (e) => {
    e.preventDefault();
    signInWithEmailAndPassword(userInfo.email, userInfo.password);
  };
  const passwordResetHandelr = async (e) => {
    if (userInfo.email) {
      await sendPasswordResetEmail(userInfo.email);
      toast("Sent email");
    }
  };

  useEffect(() => {
    console.log(error);
    if (error) {
      switch (error?.code) {
        case "auth/email-already-exists":
          toast("email already exists");
          break;
        case "auth/invalid-email":
          toast("plz provide a valid email");
          break;
        case "auth/invalid-email-verified":
          toast("invalid email verified");
          break;
        case "auth/invalid-password":
          toast("invalid password");
          break;
        default:
          toast("Something Went Wrong");
      }
    }
  }, [error]);

  useEffect(() => {
    if (user || Googleuser || userState) {
      navigate(from, { replace: true });
    }
  }, [user, Googleuser, userState]);

  return (
    <div className="infix_login_area">
      <div className="login_area_inner">
        <div className="logo_img text-center">
          <img src={logo} alt="" />
        </div>
        <h4 className="text-center">
          Welcome back, Please login to your account{" "}
        </h4>
        <div className="login_with_links">
          <button
            onClick={() => signInWithGoogle()}
            className="w-100 text-center d-flex align-items-center justify-content-center"
          >
            Login with Google
          </button>
        </div>
        <p className="sign_up_text mb_40">Or login with Email Address</p>
        <form onSubmit={loginHandler}>
          <div className="row">
            <div className="col-12 mb_35">
              <div className="input-group custom_group_field ">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <img src="img/my_account/email.svg" alt="" />
                  </span>
                </div>
                <input
                  type="email"
                  className="form-control"
                  placeholder="E.g. example@gmail.com"
                  required
                  onChange={emailHandler}
                />
              </div>
              {userError?.emailError && (
                <p className="error_text">{userError?.emailError}</p>
              )}
            </div>
            <div className="col-12 mb_40">
              <div className="input-group custom_group_field ">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <img src="img/my_account/pass.svg" alt="" />
                  </span>
                </div>
                <input
                  type="password"
                  className="form-control"
                  placeholder="Enter Password"
                  aria-label="Enter Password"
                  onChange={passwordHandler}
                  required
                />
              </div>
              {userError?.passwordError && (
                <p className="error_text">{userError?.passwordError}</p>
              )}
            </div>
            <div className="col-12">
              <button className="primary_btn large_btn w-100 text-center f_w_700">
                Sign In
              </button>
            </div>
            <div className="col-12 text-center">
              <p className="sign_up_text style2">
                Don’t have an account? <Link to="/register">Sing Up</Link>
              </p>
              <p className="sign_up_text style2">
                Forgot your password ?{" "}
                <button
                  onClick={passwordResetHandelr}
                  className="btn btn-link text-decoration-none text-capitalize p-0 shadow-none"
                >
                  {" "}
                  reset password{" "}
                </button>
              </p>
            </div>
            <ToastContainer></ToastContainer>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
