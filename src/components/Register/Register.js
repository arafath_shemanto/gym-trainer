import React, { useEffect, useState } from "react";
import {
  useCreateUserWithEmailAndPassword,
  useSignInWithGoogle,
} from "react-firebase-hooks/auth";
import { Link, useNavigate } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import logo from "../../assets/img/logo.png";
import auth from "../Firebase/Firebase";
import "react-toastify/dist/ReactToastify.css";

const Register = () => {
  // take it in state
  const [userInfo, setUserInfo] = useState({
    email: "",
    password: "",
    confirmPassword: "",
  });
  const [userError, setUserError] = useState({
    emailError: "",
    passwordError: "",
    confirmPasswordError: "",
    otherError: "",
  });
  const [signInWithGoogle, Googleuser, Googleloading, GoogleError] =
    useSignInWithGoogle(auth);
  const navigate = useNavigate();

  // create user
  const [createUserWithEmailAndPassword, user, loading, error] =
    useCreateUserWithEmailAndPassword(auth, { sendEmailVerification: true });
  // Handlers
  const emailHandler = (e) => {
    let emailRx = /\S+@\S+\.\S+/;
    let validEmail = emailRx.test(e.target.value);
    if (validEmail) {
      setUserInfo({ ...userInfo, email: e.target.value });
      setUserError({ ...userError, emailError: "" });
    } else {
      setUserInfo({ ...userInfo, email: "" });
      setUserError({ ...userError, emailError: "type valid email" });
    }
  };
  const passwordHandler = (e) => {
    let passwordRegex = /.{8,}/;
    let validPass = passwordRegex.test(e.target.value);
    if (validPass) {
      setUserInfo({ ...userInfo, password: e.target.value });
      setUserError({ ...userError, passwordError: "" });
    } else {
      setUserInfo({ ...userInfo, password: "" });
      setUserError({ ...userError, passwordError: "Must be 8 Charectar " });
    }
  };
  const confirmPassHandler = (e) => {
    if (e.target.value === userInfo.password) {
      setUserInfo({ ...userInfo, confirmPassword: e.target.value });
      setUserError({ ...userError, confirmPasswordError: "" });
    } else {
      setUserInfo({ ...userInfo, confirmPassword: "" });
      setUserError({
        ...userError,
        confirmPasswordError: "password does't matched ",
      });
    }
  };

  // submit handler
  const resgSubmitHandler = (event) => {
    event.preventDefault();
    createUserWithEmailAndPassword(userInfo.email, userInfo.password);
  };

  useEffect(() => {
    if (user) {
      toast("successfully registered");
      navigate("/");
    }
  }, [user]);

  useEffect(() => {
    if (Googleuser) {
      navigate("/");
    }
  }, [Googleuser]);

  useEffect(() => {
    if (error) {
      console.log(error);
      switch (error?.code) {
        case "auth/email-already-in-use":
          toast("email already used");
          break;
        case "auth/invalid-email":
          toast("plz provide a valid email");
          break;
        case "auth/invalid-email-verified":
          toast("invalid email verified");
          break;
        case "auth/invalid-password":
          toast("invalid password");
          break;
        default:
          toast("Something Went Wrong");
      }
    }
  }, [error]);

  if (loading) {
    return <p>Loading...</p>;
  }
  return (
    <div className="infix_login_area">
      <div className="login_area_inner">
        <div className="logo_img text-center">
          <img src={logo} alt="" />
        </div>
        <h4 className="text-center">
          Welcome! Create an account within a minute.
        </h4>
        <div className="login_with_links">
          <button
            onClick={() => signInWithGoogle()}
            className="w-100 text-center d-flex align-items-center justify-content-center"
          >
            Login with Google
          </button>
        </div>
        <p className="sign_up_text mb_20 pb-1">
          Or Register with Email Address
        </p>
        <form onSubmit={resgSubmitHandler}>
          <div className="row">
            <div className="col-12">
              <input
                name="email"
                placeholder="Type e-mail address"
                onChange={emailHandler}
                className="primary_line_input mb_10"
                required
                type="email"
              />
              {userError?.emailError && (
                <p className="error_text">{userError?.emailError}</p>
              )}
            </div>
            <div className="col-12">
              <input
                name="password"
                placeholder="Enter password"
                onChange={passwordHandler}
                className="primary_line_input mb-0"
                required
                type="text"
              />
              {userError?.passwordError && (
                <p className="error_text">{userError?.passwordError}</p>
              )}
            </div>
            <div className="col-12">
              <input
                name="password"
                onChange={confirmPassHandler}
                placeholder="Re-enter password"
                className="primary_line_input mb_10"
                required
                type="text"
              />
              {userError?.confirmPasswordError && (
                <p className="error_text">{userError?.confirmPasswordError}</p>
              )}
            </div>
            <div className="col-12 mt-4">
              {userError?.otherError && (
                <p className="error_text">{userError?.otherError}</p>
              )}
              <button className="primary_btn large_btn w-100 text-center f_w_700">
                Sign Up
              </button>
            </div>
            <div className="col-12">
              <p className="sign_up_text text-center">
                Already have an account? <Link to="/login">Login</Link>
              </p>
            </div>
            <ToastContainer></ToastContainer>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Register;
